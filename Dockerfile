FROM mcr.microsoft.com/dotnet/sdk:latest AS build-env

WORKDIR /App

ARG PACKAGE_VERSION

# Copy everything
COPY . ./

# Required for pre-release packages available from the GitLab projects. Pre-release packages are not
# currently published to nuget.org
RUN echo "adding sources core and scale" \
        &&  dotnet nuget add source "https://gitlab.com/api/v4/projects/51515286/packages/nuget/index.json" \
            --name gitlab_core \
        && dotnet nuget add source "https://gitlab.com/api/v4/projects/51838363/packages/nuget/index.json" \
            --name gitlab_scale
# Restore as distinct layers
RUN dotnet restore webapi.csproj
RUN if [ "$PACKAGE_VERSION" ]; then dotnet add package tenduke.scale --version $PACKAGE_VERSION; fi
# Build and publish a release
RUN dotnet publish -c Release -o out webapi.csproj

# Build runtime image
FROM mcr.microsoft.com/dotnet/aspnet:latest as runtime
WORKDIR /App
COPY --from=build-env /App/out .

ENTRYPOINT ["dotnet", "webapi.dll"]
