using Microsoft.AspNetCore.Mvc;
using Tenduke.Scale.LicenseCheckout;
using Tenduke.Scale.RequestOptions;
using webapi.Models;

namespace webapi.Controllers;

public class CheckoutsController : Controller
{
    private readonly ILicenseCheckoutClient _licenseCheckoutService;

    public CheckoutsController(ILicenseCheckoutClient licenseCheckoutClient)
    {
        _licenseCheckoutService = licenseCheckoutClient;
    }

    [HttpGet]
    [Route("api/licensees/{licenseeId:guid}/licenses/{licenseId:guid}/[controller]/")]
    public async Task<LicenseCheckouts> Checkouts(
        Guid licenseeId,
        Guid licenseId,
        [FromQuery] int limit = 5,
        [FromQuery] int offset = 0
    )
    {
        var apiResponse = await _licenseCheckoutService.DescribeLicenseUsageAsync(
            licenseeId,
            licenseId,
            null,
            new Pagination { Limit = limit, Offset = offset }
        );

        var checkoutModels = apiResponse.ClientBindings.Select(x => new Checkout
        {
            ClientHardwareId = x.CliHwId,
            Created = x.Created,
            ValidFrom = x.ValidFrom,
            ValidUntil = x.ValidUntil,
            LastHeartbeat = x.LastHeartbeat,
        });

        return new LicenseCheckouts(apiResponse.AllClientBindingsIncluded, checkoutModels);
    }
}
