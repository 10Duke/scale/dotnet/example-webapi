using Microsoft.AspNetCore.Mvc;
using Tenduke.Scale.LicenseCheckout;
using webapi.Models;

namespace webapi.Controllers;

public class ProductsController : Controller
{
    private readonly ILicenseCheckoutClient _licenseCheckoutClient;

    public ProductsController(ILicenseCheckoutClient licenseCheckoutClient)
    {
        _licenseCheckoutClient = licenseCheckoutClient;
    }

    [HttpGet]
    [Route("api/licensees/{licenseeId:guid}/[controller]")]
    public async Task<IEnumerable<Product>> GetProducts(Guid licenseeId)
    {
        var apiResult = await _licenseCheckoutClient.DescribeAsFeatureFlagsAsync(licenseeId, null);

        return apiResult.FeatureFlags.Select(x => new Product
        {
            ProductName = x.ProductName,
            Features = x.Features,
        });
    }
}
