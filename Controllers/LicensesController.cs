using Microsoft.AspNetCore.Mvc;
using webapi.Models;
using webapi.Services;

namespace webapi.Controllers;

public class LicensesController : Controller
{
    private readonly ILicenseService _licenseService;

    public LicensesController(ILicenseService licenseService)
    {
        _licenseService = licenseService;
    }

    [HttpGet]
    [Route("api/licensees/{licenseeId:guid}/[controller]")]
    public async Task<LicensesResult> Licenses(Guid licenseeId, [FromQuery] int offset = 0)
    {
        var model = await _licenseService.GetLicensesForLicenseeAndConsumerAsync(
            licenseeId,
            offset
        );
        return model;
    }

    [HttpGet]
    [Route("api/[controller]/{productName}/{id:guid}/checkout")]
    public async Task<string> Checkout(string productName, Guid id)
    {
        var result = await _licenseService.CheckoutAsync(id, productName);
        if (result.Success)
        {
            return result.LicenseToken!;
        }
        Response.StatusCode = 400;
        return $@"{{ ""error"": ""{result.ErrorCode}"", ""description"": ""{result.ErrorDescription}"" }}";
    }

    [HttpPost]
    [Route("api/[controller]/heartbeat")]
    public async Task<string> Heartbeat([FromForm] FileUploadModel model)
    {
        if (model?.File == null || model.File.Length <= 0)
        {
            return @"{ ""error"": ""BadRequest"", ""description"": ""No license was uploaded."" }";
        }
        using var readStream = model.File.OpenReadStream();
        using var textReader = new StreamReader(readStream);
        var content = textReader.ReadToEnd();

        var result = await _licenseService.HeartbeatAsync(content);

        if (result.Success)
        {
            return result.LicenseToken!;
        }
        Response.StatusCode = 400;
        return $@"{{ ""error"": ""{result.ErrorCode}"", ""description"": ""{result.ErrorDescription}"" }}";
    }

    [HttpPost]
    [Route("/api/[controller]/release")]
    [Produces("application/json")]
    public async Task<LicenseReleaseResult> Release([FromForm] FileUploadModel model)
    {
        if (model?.File == null || model.File.Length <= 0)
        {
            return new LicenseReleaseResult
            {
                ErrorCode = "BadRequest",
                ErrorDescription = "No license was uploaded.",
            };
        }
        using var readStream = model.File.OpenReadStream();
        using var textReader = new StreamReader(readStream);
        var content = textReader.ReadToEnd();

        return await _licenseService.ReleaseAsync(content);
    }
}
