using System.Text.Json;
using IdentityModel.OidcClient;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Tenduke.Core.Config;
using webapi.Models;
using webapi.Services;

namespace webapi.Controllers;

public class AuthController : Controller
{
    private readonly TendukeConfig _config;
    private readonly ILogger<AuthController> _logger;

    public AuthController(TendukeConfig tendukeConfig, ILogger<AuthController> logger)
    {
        _logger = logger;
        _config = tendukeConfig;
    }

    [HttpGet]
    [Route("api/[controller]/LoginUrl")]
    [AllowAnonymous]
    public async Task<string> LoginUrl()
    {
        var oidcClient = OidcClientFactory.BuildOidcClient(HttpContext.Request, _config);
        var state = await oidcClient.PrepareLoginAsync();
        var stateString = JsonSerializer.Serialize(state);
        Request.HttpContext.Session.SetString("auth_state", stateString);
        return state.StartUrl;
    }

    [HttpPost]
    [Route("api/[controller]/Login")]
    [AllowAnonymous]
    public async Task<UserInfo> Login([FromBody] AuthCode authCode)
    {
        var stateString = Request.HttpContext.Session.GetString("auth_state");
        var state = JsonSerializer.Deserialize<AuthorizeState>(stateString!);
        var oidcClient = OidcClientFactory.BuildOidcClient(HttpContext.Request, _config);
        var result = await oidcClient.ProcessResponseAsync(authCode.Data, state);
        Request.HttpContext.Session.SetString("access_token", result.AccessToken);
        Request.HttpContext.Session.SetString("id_token", result.IdentityToken);
        if (!string.IsNullOrWhiteSpace(result.RefreshToken))
        {
            Request.HttpContext.Session.SetString("refresh_token", result.RefreshToken);
        }

        var subClaim = result.User.Claims.FirstOrDefault(x => x.Type == "sub");
        Request.HttpContext.Session.SetString("sub", subClaim!.Value);
        var nameClaim = result.User.Claims.FirstOrDefault(x => x.Type == "name");
        var givenNameClaim = result.User.Claims.FirstOrDefault(x => x.Type == "given_name");
        var familyNameClaim = result.User.Claims.FirstOrDefault(x => x.Type == "family_name");
        var emailClaim = result.User.Claims.FirstOrDefault(x => x.Type == "email");

        return new UserInfo
        {
            Name = nameClaim?.Value,
            GivenName = givenNameClaim?.Value,
            FamilyName = familyNameClaim?.Value,
            Email = emailClaim?.Value,
        };
    }

    [HttpPost]
    [Route("api/[controller]/Logout")]
    public async Task Logout()
    {
        var stateString = Request.HttpContext.Session.GetString("auth_state");
        try
        {
            var oidcClient = OidcClientFactory.BuildOidcClient(HttpContext.Request, _config);
            await oidcClient.LogoutAsync(new LogoutRequest { State = stateString });
        }
        catch (InvalidOperationException ex)
        {
            _logger.LogWarning("Exception attempting to end session: {msg}", ex.Message);
        }
        Request.HttpContext.Session.Clear();
    }
}
