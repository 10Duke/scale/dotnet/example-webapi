using Microsoft.AspNetCore.Mvc;
using webapi.Models;
using webapi.Services;

namespace webapi.Controllers;

public class LicenseesController : Controller
{
    private readonly ILicenseeService _licenseeService;

    public LicenseesController(ILicenseeService licenseeService)
    {
        _licenseeService = licenseeService;
    }

    [Route("api/[controller]")]
    [Route("api/[controller]/[action]")]
    [HttpGet]
    public Task<IEnumerable<Licensee>> Index()
    {
        return _licenseeService.GetAllAsync();
    }
}
