# Compose stack for 10Duke Scale .NET SDK

This docker compose file uses the frontend and a .NET backend built using the 10Duke Scale SDK for
.NET, with a NGINX reverse proxy handling TLS termination.

## Configuration

See `.env.example`.

Copy this file to `.env` and edit the values for your scenario.

Alternatively you can use `.env.example` as a source of the environment variables to set and set
them in the shell or from the CLI.

```sh
docker compose run -e TENDUKESCALE__LICENSINGAPIURL=https://example.lic-api.scale.10duke.com
```

This might be feasible for a continuous integration (or other scripted) scenario where the
configuration is dynamically generated but typically a `.env` file will be more practical.

## Building and using the docker compose stack

There are two pre-requisites to consider before starting the docker compose stack.

Before the stack can be brought up, there needs to be a self-signed certificate and key for the
reverse proxy to use.

If the stack is to be run with pre-release versions of Tenduke.Scale and/or Tenduke.Core nuget
packages, then credentials for the GitLab package registry must be provided as build arguments for
the docker compose stack. If stable versions from nuget.org are used, this step can be ignored.

### Generating self-signed certificate

Running locally, use `openssl` to generate a key pair in the `./cert` folder.

```sh
mkdir cert

cd cert

openssl req -x509 -newkey rsa:4096 \
  -keyout nginx-selfsigned.key \
  -out nginx-selfsigned.crt \
  -days 365 -nodes
```

You will be prompted for additional details for the key.

If you use different folder or file names than shown here, you will need to edit the `compose.yaml`
file to provide the absolute paths to the files in the `volumes` entries for the `nginx` service.

### Providing credentials for the GitLab package registry

Locally, provide your own credentials (use a personnel access token - we recommended adding this to
the `.env` file, add `GITLAB_USER` and `GITLAB_PASSWORD` to `.env` before building):

```sh
GITLAB_USER=your-gitlab-username
GITLAB_PASSWORD=your-gitlab-personal-access-token
```

```sh
docker compose build
```

For GitLab pipelines, the CI Job token can be used:

```sh
GITLAB_USER=gitlab-ci-token GITLAB_PASSWORD=$CI_JOB_TOKEN docker compose build
```

## Start the stack

```sh
docker compose up
```

Then you should be able to connect to the frontend at `https://localhost:8091`.

The backend API is served at `https://localhost:8091/api`.

The first time you access the service, you will need to trust the self signed certificate in your
browser. This will need to be repeated if you rebuild the docker image with a different TLS
certificate.
