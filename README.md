# .NET example Web API using 10Duke Scale SDK for .NET

This project is an ASP.NET core web API built on top of the .NET implementation of the 10Duke Scale
SDK.

The API is the (.NET version of the) backend-for-frontend for [an example SPA](https://gitlab.com/10Duke/core/examples/example-webapp)
that implements a 'My Licenses' browser based app. The project covers the element labeled "Backend"
in the diagram below. It contains the implementation of an API that uses the 10Duke Scale SDK for
.NET to talk to the 10Duke Scale API.

```mermaid
flowchart LR
    Frontend-->Backend
    subgraph Backend
        App["app logic"] -->SDK
    end
    Backend-->ScaleAPI
    style Backend fill:#696
```

## Setup

Restore the tools for the project:

```sh
dotnet tool restore
```

You will need to copy [the example appsettings.json file](./appsettings.json.example) to
`appsettings.json` and edit to provide the required settings for the 10Duke Scale API host, the OIDC
provider, and the location for the frontend.

Alternatively, these settings can be provided as environment variables.

## Running for development

See the [frontend project](https://gitlab.com/10Duke/core/examples/example-webapp) for instructions
to run the frontend.

This frontend project needs to be configured with the URL for the API in this project (e.g.
`https://localhost:7018/api` see `VITE_API_URL` in .env file) and this project is configured with
the  `CorsOrigin` setting in `appsettings.json` matching the host and port that the frontend is
running on.

Run both this project and the frontend project.

## Code formatting

The project uses CSharpier for code formatting.

The formatting rules can be applied to the code by running:

```sh
dotnet dotnet-csharpier .
```
