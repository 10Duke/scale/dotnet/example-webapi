using System.Security.Claims;
using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.JsonWebTokens;
using Tenduke.Core.Config;
using webapi.Services;

namespace webapi.Auth;

internal class SessionTokenAuthenticationSchemeHandler
    : AuthenticationHandler<SessionTokenAuthenticationSchemeOptions>
{
    private readonly IHttpContextAccessor _contextAccessor;
    private readonly TimeProvider _timeProvider;
    private readonly TendukeConfig _config;

    public SessionTokenAuthenticationSchemeHandler(
        TendukeConfig tendukeConfig,
        IHttpContextAccessor contextAccessor,
        IOptionsMonitor<SessionTokenAuthenticationSchemeOptions> options,
        ILoggerFactory logger,
        UrlEncoder encoder,
        TimeProvider timeProvider
    )
        : base(options, logger, encoder)
    {
        _config = tendukeConfig;
        _contextAccessor = contextAccessor;
        _timeProvider = timeProvider;
    }

    protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
    {
        var idToken = _contextAccessor.HttpContext?.Session.GetString("id_token");
        if (!string.IsNullOrWhiteSpace(idToken))
        {
            var handler = new JsonWebTokenHandler();
            var token = handler.ReadJsonWebToken(idToken);

            var claims = new[] { new Claim(ClaimTypes.Name, "Test") };
            var principal = new ClaimsPrincipal(new ClaimsIdentity(claims, "Tokens"));
            var ticket = new AuthenticationTicket(principal, this.Scheme.Name);

            if (token.ValidTo > _timeProvider.GetUtcNow())
            {
                return AuthenticateResult.Success(ticket);
            }

            // attempt to get a new id_token?
            var refresh = _contextAccessor.HttpContext?.Session.GetString("refresh_token");
            if (!string.IsNullOrWhiteSpace(refresh))
            {
                var oidcClient = OidcClientFactory.BuildOidcClient(
                    _contextAccessor.HttpContext?.Request!,
                    _config
                );
                var newToken = await oidcClient.RefreshTokenAsync(refresh);
                Request.HttpContext.Session.SetString("access_token", newToken.AccessToken);
                Request.HttpContext.Session.SetString("id_token", newToken.IdentityToken);
                if (!string.IsNullOrWhiteSpace(newToken.RefreshToken))
                {
                    Request.HttpContext.Session.SetString("refresh_token", newToken.RefreshToken);
                }
                return AuthenticateResult.Success(ticket);
            }
        }

        return AuthenticateResult.Fail("No valid authentication provided.");
    }
}
