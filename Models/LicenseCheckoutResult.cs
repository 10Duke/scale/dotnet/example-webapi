namespace webapi.Models;

public class LicenseCheckoutResult
{
    public string? LeaseId { get; init; }
    public bool Success { get; init; }
    public string? ErrorCode { get; init; }
    public string? ErrorDescription { get; init; }
    public string? LicenseToken { get; init; }
}
