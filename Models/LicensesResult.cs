namespace webapi.Models;

public class LicensesResult
{
    public IEnumerable<License> Licenses { get; }

    public bool AllLicensesLoaded { get; }

    internal LicensesResult(IEnumerable<License> licenses, bool allLoaded)
    {
        Licenses = licenses;
        AllLicensesLoaded = allLoaded;
    }
}
