namespace webapi.Models;

public class LicenseReleaseResult
{
    public bool Success { get; set; }
    public string? ErrorCode { get; init; }
    public string? ErrorDescription { get; init; }
    public string? LicenseToken { get; init; }
}
