namespace webapi.Models;

public class AuthRequestDetails
{
    public string? Uri { get; set; }
    public string? State { get; set; }
}
