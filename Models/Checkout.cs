namespace webapi.Models;

public class Checkout
{
    public DateTimeOffset? Created { get; set; }
    public DateTimeOffset? ValidFrom { get; set; }
    public DateTimeOffset? ValidUntil { get; set; }
    public DateTimeOffset? LastHeartbeat { get; set; }
    public string? ClientHardwareId { get; set; }
}
