namespace webapi.Models;

public class License
{
    public Guid? Id { get; init; }
    public string? ProductName { get; init; }

    public DateTimeOffset ValidFrom { get; init; }

    public DateTimeOffset? ValidTo { get; init; }

    public Guid? LicenseeId { get; init; }

    public IEnumerable<Checkout>? Checkouts { get; init; }

    public bool AllCheckoutsLoaded { get; init; }

    public static License FromApiModel(Tenduke.Scale.Licensing.License apiModel, Guid licenseeId)
    {
        return new License
        {
            Id = apiModel.Id,
            ProductName = apiModel.ProductName,
            ValidFrom = apiModel.ValidFrom,
            ValidTo = apiModel.ValidUntil,
            LicenseeId = licenseeId,
            AllCheckoutsLoaded = false,
            Checkouts = Enumerable.Empty<Checkout>(),
        };
    }
}
