namespace webapi.Models;

public class LoginResult
{
    public bool IsLoggedIn { get; set; }
    public UserInfo? UserInfo { get; set; }
    public string? Error { get; set; }
}
