namespace webapi.Models;

public class LicenseCheckouts
{
    public IEnumerable<Checkout> Checkouts { get; }

    public bool AllCheckoutsLoaded { get; }

    public LicenseCheckouts(bool allIncluded, IEnumerable<Checkout> checkouts)
    {
        AllCheckoutsLoaded = allIncluded;
        Checkouts = checkouts;
    }
}
