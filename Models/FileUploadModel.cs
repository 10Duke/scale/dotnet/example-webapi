namespace webapi.Models;

public class FileUploadModel
{
    public required IFormFile File { get; set; }
}
