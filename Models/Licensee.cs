using Tenduke.Scale.Licensing;

namespace webapi.Models;

public class Licensee
{
    public Guid? Id { get; init; }
    public string? Name { get; init; }
    public string? Description { get; init; }

    public bool IsPerson { get; init; }

    public static Licensee FromApiModel(Tenduke.Scale.Licensing.Licensee apiModel)
    {
        return new Licensee
        {
            Id = apiModel.Id,
            Name = apiModel.Name,
            Description = apiModel.Description,
            IsPerson = apiModel.Type == LicenseeType.Person,
        };
    }
}
