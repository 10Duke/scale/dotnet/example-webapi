namespace webapi.Models;

public class Product
{
    public string? ProductName { get; init; }
    public IEnumerable<string>? Features { get; init; }
}
