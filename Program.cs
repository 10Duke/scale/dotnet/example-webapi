using Microsoft.AspNetCore.Authorization;
using Tenduke.Core.Config;
using Tenduke.Scale.Extensions.DependencyInjection;
using webapi.Auth;
using webapi.Middleware;
using webapi.Services;

var builder = WebApplication.CreateBuilder(args);
builder.Configuration.AddEnvironmentVariables();

var corsAcceptOrigin = builder.Configuration.GetValue<string>("CorsOrigin");

// Add services to the container.
builder.Services.AddCors(options =>
{
    options.AddPolicy(
        name: "frontendcorspolicy",
        policy =>
        {
            policy
                .WithOrigins(corsAcceptOrigin!)
                .AllowAnyMethod()
                .AllowCredentials()
                .WithHeaders("authorization", "accept", "content-type", "origin");
        }
    );
});

builder
    .Services.AddAuthentication()
    .AddScheme<SessionTokenAuthenticationSchemeOptions, SessionTokenAuthenticationSchemeHandler>(
        "SessionTokens",
        opts => { }
    );

builder.Services.AddAuthorization(options =>
{
    options.DefaultPolicy = new AuthorizationPolicyBuilder()
        .AddAuthenticationSchemes("SessionTokens")
        .RequireAuthenticatedUser()
        .Build();
});

builder.Services.AddDistributedMemoryCache();
builder.Services.AddSession(options =>
{
    options.IdleTimeout = TimeSpan.FromMinutes(10);
    options.Cookie.HttpOnly = true;
    options.Cookie.SecurePolicy = CookieSecurePolicy.Always;
    options.Cookie.IsEssential = true;
    options.Cookie.SameSite = SameSiteMode.None;
});

builder.Services.AddControllersWithViews();

// use the IdTokenProvider service in this app so id_token is pulled from current auth cookie
builder.Services.ConfigureTendukeScaleForIdToken<IdTokenProvider>(b =>
    b.WithApplication("awesomeapp-spa", "0.1")
);

builder.Services.AddHttpContextAccessor();

// register services used by the controllers (wrappers for ILicenseCheckoutClient)
builder.Services.AddTransient<ILicenseeService, LicenseeService>();
builder.Services.AddTransient<ILicenseService, LicenseService>();

var app = builder.Build();

var tendukeConfigBuilder = app.Services.GetRequiredService<TendukeConfigurationBuilder>();

await tendukeConfigBuilder
    .AddConfigurationSection(builder.Configuration, "TendukeScale")
    .AddEnvironmentVariables("TENDUKESCALE")
    .AddOidcDiscoveryLoader()
    .BuildAsync();

app.UseSession();
app.UseHttpsRedirection();
app.UseRouting();
app.UseCors("frontendcorspolicy");
app.UseAuthentication();
app.UseAuthorization();
app.UseMiddleware<ExceptionMiddleware>();

app.UseStaticFiles();
app.MapControllers().RequireAuthorization();
app.MapFallbackToFile("index.html");

app.Run();
