namespace webapi.Middleware;

public class ErrorMessage
{
    public int StatusCode { get; set; }
    public string? Error { get; set; }
    public string? Description { get; set; }
}
