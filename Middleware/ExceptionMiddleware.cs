using System.Net;
using System.Text;
using System.Text.Json;
using Tenduke.Core.Exceptions;

namespace webapi.Middleware;

public class ExceptionMiddleware
{
    private readonly RequestDelegate _next;

    public ExceptionMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task InvokeAsync(HttpContext httpContext)
    {
        try
        {
            await _next(httpContext);
        }
        catch (Exception ex)
        {
            var message = MapException(ex);
            var options = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
            };
            var bodyText = JsonSerializer.Serialize(message, options);
            var bodyBytes = Encoding.UTF8.GetBytes(bodyText);
            httpContext.Response.StatusCode = message.StatusCode;
            await httpContext.Response.BodyWriter.WriteAsync(bodyBytes);
        }
    }

    private static ErrorMessage MapException(Exception ex)
    {
        if (ex is ApiException error)
        {
            return new ErrorMessage
            {
                Error = error.Error,
                Description = error.Description,
                StatusCode = (int)error.StatusCode,
            };
        }
        return new ErrorMessage
        {
            Error = "Unknown error",
            Description = ex.Message,
            StatusCode = (int)HttpStatusCode.InternalServerError,
        };
    }
}
