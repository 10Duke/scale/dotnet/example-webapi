using Tenduke.Scale.LicenseCheckout;
using Tenduke.Scale.RequestOptions;
using webapi.Models;

namespace webapi.Services;

public class LicenseeService : ILicenseeService
{
    private readonly ILicenseCheckoutClient _client;

    public LicenseeService(ILicenseCheckoutClient licenseCheckoutClient)
    {
        _client = licenseCheckoutClient;
    }

    public async Task<IEnumerable<Licensee>> GetAllAsync()
    {
        var licenseesToReturn = new List<Licensee>();
        int offset = 0;
        var more = true;
        while (more)
        {
            var (licensees, moreToRead) = await ReadFiveLicensees(offset);
            more = moreToRead;
            offset += licensees.Count();
            licenseesToReturn.AddRange(licensees);
        }
        return licenseesToReturn;
    }

    private async Task<(IEnumerable<Licensee>, bool)> ReadFiveLicensees(int offset)
    {
        var pagination = new Pagination
        {
            Limit = 6,
            Offset = offset,
            OrderAscending = true,
            OrderBy = "name",
        };
        var apiModels = await _client.DescribeLicenseesAsync(pagination);
        var more = apiModels.Count() > 5;
        return (apiModels.Take(5).Select(x => Licensee.FromApiModel(x)), more);
    }
}
