using webapi.Models;

namespace webapi.Services;

public interface ILicenseService
{
    Task<LicensesResult> GetLicensesForLicenseeAndConsumerAsync(Guid licenseeId, int offset);
    Task<LicenseCheckoutResult> CheckoutAsync(Guid licenseId, string productName);
    Task<LicenseCheckoutResult> HeartbeatAsync(string licenseTokenContent);
    Task<LicenseReleaseResult> ReleaseAsync(string licenseTokenContent);
}
