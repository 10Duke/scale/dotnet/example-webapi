using Microsoft.AspNetCore.Authentication;
using Tenduke.Core.Auth.TokenStore;

namespace webapi.Services;

public class IdTokenProvider : IIdTokenProvider
{
    private readonly IHttpContextAccessor _accessor;
    private readonly ILogger<IdTokenProvider> _logger;

    public IdTokenProvider(ILogger<IdTokenProvider> logger, IHttpContextAccessor accessor)
    {
        _accessor = accessor;
        _logger = logger;
    }

    public Task<string> GetIdTokenAsync()
    {
        var context = _accessor.HttpContext;
        if (context is null)
        {
            return Task.FromResult(string.Empty);
        }
        var idToken = context.Session.GetString("id_token");
        _logger.LogInformation("id_token: {idToken}", idToken);
        return Task.FromResult(idToken!);
    }
}
