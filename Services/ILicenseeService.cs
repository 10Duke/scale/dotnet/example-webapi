using webapi.Models;

namespace webapi.Services;

public interface ILicenseeService
{
    Task<IEnumerable<Licensee>> GetAllAsync();
}
