using System.IdentityModel.Tokens.Jwt;
using Tenduke.Scale.LicenseCheckout;
using Tenduke.Scale.RequestOptions;
using webapi.Models;

namespace webapi.Services;

public class LicenseService(ILicenseCheckoutClient licenseCheckoutClient) : ILicenseService
{
    private readonly ILicenseCheckoutClient _client = licenseCheckoutClient;

    public async Task<LicenseCheckoutResult> CheckoutAsync(Guid licenseId, string productName)
    {
        var checkoutResults = await _client.CheckoutAsync(
            new[]
            {
                new LicenseCheckoutArguments { ProductName = productName, LicenseId = licenseId },
            },
            new ClientDetails { HardwareId = "web-1234" }
        );
        var firstResult = checkoutResults.FirstOrDefault();
        return new LicenseCheckoutResult
        {
            Success = firstResult!.IsSuccess,
            LeaseId = firstResult!.LicenseToken?.LeaseId,
            LicenseToken = firstResult.RawJwt,
            ErrorCode = firstResult!.ErrorCode,
            ErrorDescription = firstResult!.ErrorDescription,
        };
    }

    public async Task<LicensesResult> GetLicensesForLicenseeAndConsumerAsync(
        Guid licenseeId,
        int offset
    )
    {
        var apiModels = await _client.DescribeLicensesAsync(
            licenseeId,
            new DescribeOptions { WithMetadata = false },
            new Pagination { Limit = 6, Offset = offset }
        );

        var licenses = apiModels
            .Licenses.Take(5)
            .Select(x => License.FromApiModel(x, licenseeId))
            .ToArray();

        return new LicensesResult(licenses, apiModels.Licenses.Count() <= 5);
    }

    public async Task<LicenseCheckoutResult> HeartbeatAsync(string licenseTokenContent)
    {
        var leaseId = GetLeaseId(licenseTokenContent);
        var results = await _client.HeartbeatAsync(
            new[] { new LicenseHeartbeatArguments { LeaseId = leaseId } }
        );
        var firstResult = results.First();
        return new LicenseCheckoutResult
        {
            Success = firstResult!.IsSuccess,
            LeaseId = firstResult!.LicenseToken?.LeaseId,
            LicenseToken = firstResult.RawJwt,
            ErrorCode = firstResult!.ErrorCode,
            ErrorDescription = firstResult!.ErrorDescription,
        };
    }

    public async Task<Models.LicenseReleaseResult> ReleaseAsync(string licenseTokenContent)
    {
        var leaseId = GetLeaseId(licenseTokenContent);
        var results = await _client.ReleaseAsync(
            new[] { new LicenseReleaseArguments { LeaseId = leaseId } }
        );
        var firstResult = results.First();
        return new Models.LicenseReleaseResult
        {
            Success = firstResult!.Released,
            ErrorCode = firstResult!.ErrorCode,
            ErrorDescription = firstResult!.ErrorDescription,
        };
    }

    private string GetLeaseId(string licenseTokenContent)
    {
        var handler = new JwtSecurityTokenHandler();
        var jwtSecurityToken = handler.ReadJwtToken(licenseTokenContent);
        return jwtSecurityToken.Claims.First(claim => claim.Type == "leaseId").Value;
    }
}
