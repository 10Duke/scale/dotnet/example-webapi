namespace webapi.Services;

using IdentityModel.Client;
using IdentityModel.OidcClient;
using Tenduke.Core.Config;

internal static class OidcClientFactory
{
    internal static OidcClient BuildOidcClient(HttpRequest request, TendukeConfig config)
    {
        var redirectUri = BuildRedirectUri(request, config);
        return new OidcClient(
            new OidcClientOptions
            {
                Authority = config.IdpOidcDiscoveryUrl!.AbsoluteUri,
                ClientId = config.IdpOAuthClientId,
                ClientSecret = config.IdpOAuthClientSecret,
                RedirectUri = redirectUri.AbsoluteUri,
                Scope = "openid profile email",
                Policy = new Policy
                {
                    Discovery = new DiscoveryPolicy
                    {
                        ValidateEndpoints = false,
                        ValidateIssuerName = false,
                    },
                },
            }
        );
    }

    private static Uri BuildRedirectUri(HttpRequest request, TendukeConfig tendukeConfig)
    {
        if (tendukeConfig.AuthRedirectUri is not null)
        {
            return tendukeConfig.AuthRedirectUri;
        }

        var host = request.Headers.Referer.FirstOrDefault();
        if (string.IsNullOrEmpty(host))
        {
            host = "http://localhost";
        }
        // strip potential path elements from referer URL
        var hostUri = new Uri(host);
        return new Uri(
            new Uri($"{hostUri.Scheme}://{hostUri.Authority}"),
            tendukeConfig.AuthRedirectPath
        );
    }
}
